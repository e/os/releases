Please welcome /e/OS 2.8! :rocket:

We are proud to deliver the /e/OS 2.8 version for supported devices whether they are on an official or a community build.\
Enjoy all the new features and improvements it embeds!

## ✨ We embedded some improvements! 

:man_in_tuxedo_tone3: **Account Manager**

- A link to Account Manager's Privacy Policy is now available

:shopping_bags: **App Lounge**

- The warning message appearing when manually updating App Lounge in App Lounge has been improved
- F-Droid apps' description is now formatted and easier to read


## 🕙 Software updates

➕ **LineageOS 21** latest bug fixes and security updates have been merged ([list](https://review.lineageos.org/q/branch:lineage-21.0+status:merged+after:%222024-12-25+19:00:00+%252B0100%22+before:%222025-01-29+06:44:00+%252B0100%22))

➕ **Browser** has been updaded to upstream version [v132.0.6834.83](https://github.com/uazo/cromite/releases/tag/v132.0.6834.83-36e27b9c60c551348fe4e65a537d16c76d9b48e8)

➕ **Maps** has been updated to upstream version `7.1.24.44.112F2A33.FF608AF0`

➕  **Nothing CMF Phone 1**'s firmware has been updated to `Tetris_U2.6-241204-2338_2.6`

## 🐛 Bug fixes

:shopping_bags: **App Lounge**

- Privacy ratings now load properly

:iphone: **Fairphone 3**

- Installation of the Fairphone 3's /e/OS community version based on AOSP 14 now works

:iphone: **Fairphone 4**

- QR code activity in Opera browser is now functional and able to capture links

:iphone: **Google Pixel 5**

- `Recovery.zip` is now available for community builds based on AOSP 14

:iphone: **Nothing CMF Phone 1**

- `Tap to check` is now working
- Display freeze has been fixed by disabling dynamic display: refresh rate is now 60Hz by default and can be changed in the Settings

:iphone: **Samsung devices**

- Signature mismatch preventing sideload/OTA updates is now fixed

:sunrise: **Launcher**

- Notification badge position has been fixed
- Hotseat is now vertically centered
- Taskbar is now aligned to bottom for tablets
- Badge indicating where a PWA was installed from has been removed
- Blur effect on dock's background has been improved 

:love_letter: **Mail**

- Logging into a Yahoo account in Account Manager now also applies in Mail

:children_crossing: **Parental Control**

- Parental Control now works as expected when user has logged into accounts prior to activating Parental Control
- Warning dialog informing multi users are not supported with Parental Control can now be discarded

🦸 **Third Party Apps support**

- TK app is now working properly

## ⚠ Security fixes

This /e/OS 2.8 version includes the [Android security patches](https://source.android.com/docs/security/bulletin/) available as of January 2025. 
